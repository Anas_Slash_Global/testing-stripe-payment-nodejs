const express = require("express");
const app = express();
// use alternate localhost and the port Heroku assigns to $PORT
const host = '0.0.0.0';
const port = process.env.PORT || 4242;

// This is your real test secret API key.
const stripe = require("stripe")("sk_test_51I4TrXIxU4GBTGuBvNZkcianxeVOFPmXWsrsUndR7BqA5S2hQJmBPzWfFfMmVcivuu1FNd8bmxpkOJvKv8dR8C9M00E8FePqQP");
app.use(express.static("."));
app.use(express.json());

const calculateOrderAmount = items => {
  // Replace this constant with a calculation of the order's amount
  // Calculate the order total on the server to prevent
  // people from directly manipulating the amount on the client
  //return 1400;
  console.log("printing amount here: ")
  console.log(items[0].amount);
  return items[0].amount;
};

const getName = card => {
  console.log(card[0].customer_name);
  return card[0].customer_name;
};
const getAddress = card => {
  console.log(card[0].address_country);
  return card[0].address_country;
};
app.post("/create-payment-intent", async (req, res) => {
  const { amount } = req.body;
  const { currency } = req.body;
  const { name } = req.body;
  //const { metadata } = req.body;
  
  console.log("printing items Here: ")
  console.log(amount);
  console.log(currency);
  console.log(name);

  // Create a PaymentIntent with the order amount and currency
  const paymentIntent = await stripe.paymentIntents.create({
    amount: amount, //calculateOrderAmount(items),
    currency: currency,
    metadata:{
      "name": name,
    }
    
  });
  console.log("Reached here")
  
  res.send({
    clientSecret: paymentIntent.client_secret,
    id : paymentIntent.id,
//    totalresponse: paymentIntent.charges.data[0].id
  });
});

app.post("/update-meta", async(req,res) =>{
const transaction_id = req.body;
console.log(transaction_id);
var tid = JSON.stringify(transaction_id);
console.log(tid)

const paymentIntent = await stripe.paymentIntents.update(
'pi_1I7OACIxU4GBTGuBwYbHDGru',
  {metadata: {order_id: '6735'}}
);


  res.send({
    paymentintentreponse: paymentIntent
  });

});


app.post("/refund-payment", async (req, res) => {
  const { trans_id } = req.body;
  const { refund_amount } = req.body;

  console.log(trans_id)
  console.log(refund_amount)
  console.log("I am getting Here")

  const refund = await stripe.refunds.create({
    payment_intent : trans_id,
    amount : refund_amount
  });
  res.send({
    refundresponse : refund
  })
});

app.get("/getit", async (req, res) => {
res.send('Hello I am Here');     
});

app.listen(port, host, () => console.log('Node server listening on port 4242!'));